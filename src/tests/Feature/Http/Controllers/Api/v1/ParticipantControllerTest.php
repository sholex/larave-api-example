<?php

namespace Tests\Feature\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\ParticipantController;
use App\Models\Participant;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Passport;
use Tests\TestCase;


class ParticipantControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    private $participantStructure = [
        'id',
        'first_name',
        'last_name',
        'email',
        'event_id',
        'created_at',
        'updated_at',
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        Artisan::call('db:seed', ['--class' => \EventsSeeder::class]);
        Artisan::call('db:seed', ['--class' => \ParticipantsSeeder::class]);
    }


    /**
     * @dataProvider routesProvider()
     * @param $route
     * @param $method
     * @param null $id
     */
    public function testUnauthenticated($route, $method, $id = null): void
    {
        $response = $this->json($method, route($route, $id), []);
        $response->assertUnauthorized();
    }

    /**
     * @covers       ParticipantController::index
     * @dataProvider indexProvider()
     * @param array $params
     */
    public function testIndex(array $params = []): void
    {
        $expectedCount = 10;

        Passport::actingAs($this->user);

        Participant::truncate();

        factory(Participant::class, $expectedCount)->create($params);

        $response = $this->get(route('v1.participants.index', $params) );

        $response->assertOk();

        $response->assertJsonFragment([
            'success' => true,
        ]);

        $response->assertJsonStructure([
            'success',
            'message',
            'data' => [
                '*' => $this->participantStructure
            ]
        ]);

        $response->assertJsonCount($expectedCount, 'data');
    }

    /**
     * @covers       ParticipantController::update
     * @dataProvider positiveUpdateProvider()
     * @param array $data
     */
    public function testUpdatePositive(array $data): void
    {
        Passport::actingAs($this->user);

        $participant = Participant::inRandomOrder()->limit(1)->first();

        $response = $this->json(Request::METHOD_PUT, route('v1.participants.update', $participant), $data);

        $response->assertJsonFragment([
            'success' => true,
        ]);

        $this->assertDatabaseHas('participants', $data);

        $response->assertStatus(200);
    }

    /**
     * @covers       ParticipantController::update
     * @dataProvider negativeUpdateProvider()
     * @param array $data
     */
    public function testUpdateNegative(array $data): void
    {
        Passport::actingAs($this->user);

        $participant = Participant::inRandomOrder()->limit(1)->first();

        $response = $this->json(Request::METHOD_PUT, route('v1.participants.update', $participant), $data);

        $response->assertJsonFragment([
            'success' => false,
        ]);

        $this->assertDatabaseMissing('participants', $data);

        $response->assertStatus(422);
    }

    /**
     * @covers       ParticipantController::store
     * @dataProvider positiveStoreProvider()
     * @param array $data
     */
    public function testStorePositive(array $data): void
    {
        Passport::actingAs($this->user);

        $response = $this->json(Request::METHOD_POST, route('v1.participants.store'), $data);

        $response->assertJsonFragment([
            'success' => true,
        ]);

        $this->assertDatabaseHas('participants', $data);

        $response->assertStatus(201);
    }

    /**
     * @covers       ParticipantController::store
     * @dataProvider negativeStoreProvider()
     * @param array $data
     */
    public function testStoreNegative(array $data): void
    {
        Passport::actingAs($this->user);

        $response = $this->json(Request::METHOD_POST, route('v1.participants.store'), $data);

        $response->assertJsonFragment([
            'success' => false,
        ]);

        $this->assertDatabaseMissing('participants', $data);

        $response->assertStatus(422);
    }

    /**
     * @covers ParticipantController::show
     */
    public function testShowPositive(): void
    {
        Passport::actingAs($this->user);

        $participant = Participant::inRandomOrder()->limit(1)->first();

        $response = $this->json(Request::METHOD_GET, route('v1.participants.show', $participant->id));

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'success',
            'message',
            'data' => $this->participantStructure
        ]);

    }

    /**
     * @covers ParticipantController::destroy
     */
    public function testDestroy(): void
    {
        Passport::actingAs($this->user);

        $participant = Participant::inRandomOrder()->limit(1)->first();

        $response = $this->json(Request::METHOD_DELETE, route('v1.participants.destroy', $participant));

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'success' => true,
        ]);

        $this->assertDatabaseMissing('participants', $participant->toArray());
    }


    //DataProviders

    public function positiveUpdateProvider() : array
    {
        return [
            [
                'data' => [
                    'first_name' => 'Updated first name'
                ]
            ],
            [
                'data' => [
                    'last_name' => 'Updated first name'
                ]
            ],
            [
                'data' => [
                    'email' => 'updatedemail@example.com'
                ]
            ],
            [
                'data' => [
                    'event_id' => 1
                ]
            ]
        ];
    }

    public function negativeUpdateProvider() : array
    {
        return [
            [
                'data' => [
                    'email' => 'not_valid_email'
                ]
            ]
        ];
    }

    public function positiveStoreProvider() : array
    {
        return [
            [
                'data' => [
                    'first_name' => 'Created first name',
                    'last_name' => 'Created first name',
                    'email' => 'new.unique.email@example.com',
                    'event_id' => 1
                ]
            ]
        ];
    }

    public function negativeStoreProvider() : array
    {
        return [
            'no-email' => [
                'data' => [
                    'first_name' => 'Created first name',
                    'last_name' => 'Created first name',
                    'event_id' => 1
                ]
            ],
            'no-first_name' => [
                'data' => [
                    'last_name' => 'Created first name',
                    'email' => 'new.unique.email@example.com',
                    'event_id' => 1
                ]
            ],
            'no-last_name' => [
                'data' => [
                    'first_name' => 'Created first name',
                    'email' => 'new.unique.email@example.com',
                    'event_id' => 1
                ]
            ],
            'no-event_id' => [
                'data' => [
                    'first_name' => 'Created first name',
                    'last_name' => 'Created first name',
                    'email' => 'new.unique.email@example.com',
                ]
            ],
            'not-valid-email' => [
                'data' => [
                    'first_name' => 'Created first name',
                    'last_name' => 'Created first name',
                    'email' => 'not-valid-email',
                    'event_id' => 1
                ]
            ],
        ];
    }

    public function routesProvider(): array
    {
        return [
            'route.index' => [
                'route' => 'v1.participants.index',
                'method' => Request::METHOD_GET,
            ],
            'route.store' => [
                'route' => 'v1.participants.store',
                'method' => Request::METHOD_POST,
            ],
            'route.show' => [
                'route' => 'v1.participants.show',
                'method' => Request::METHOD_GET,
                'id' => 1
            ],
            'route.update' => [
                'route' => 'v1.participants.update',
                'method' => Request::METHOD_PUT,
                'id' => 1
            ],
            'route.destroy ' => [
                'route' => 'v1.participants.destroy',
                'method' => Request::METHOD_DELETE,
                'id' => 1
            ]
        ];
    }

    public function indexProvider() : array
    {
        return [
            'no-params' => [
                'params' => [],
            ],
            'filter.event_id' => [
                'params' => [
                    'event_id' => 1
                ],
            ],


        ];
    }
}
