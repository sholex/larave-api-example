<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('root')
            ],
        ];

        foreach ($admins as $data)
        {
            User::create($data);
        }

        User::factory()->count(50)->create();
    }
}
