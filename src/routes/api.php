<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\ProductController;
use App\Http\Controllers\Api\v1\BrandController;


//"fideloper/proxy": "^4.2",
//        "fruitcake/laravel-cors": "^2.0",
//        "spatie/laravel-data": "^3.1.1"
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->name('v1.')->group(function () {
    Route::post('/register', 'App\Http\Controllers\Api\v1\AuthController@register')->name('register');
    Route::post('/login', 'App\Http\Controllers\Api\v1\AuthController@login')->name('login');

    Route::middleware('auth:api')->group(function () {
        Route::apiResource('products', ProductController::class);
        Route::apiResource('brands',BrandController::class);
    });
});
