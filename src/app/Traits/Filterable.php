<?php

declare(strict_types=1);

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait Filterable
{
    /**
     * @param Builder $builder
     * @param array $filters
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        /** @var Model $this */

        if(!$filters) {
            return $builder;
        }

        $fillableFields = $this->fillable;

        foreach ($filters as $field => $value) {
            if(!in_array($field, $fillableFields) || !$value) {
                continue;
            }
            $builder->where($field, $value);
        }

        return $builder;
    }
}
