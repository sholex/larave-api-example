<?php

declare(strict_types=1);

namespace App\Services\Product;

use App\Http\Requests\Product\Dto\CreateProductDto;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Product;
use App\Models\ProductAttributeValue;
use App\Services\ModelCreatorInterface;

//class ProductCreator implements ModelCreatorInterface
class ProductCreator
{
    public function create(CreateProductDto $dto): Product
    {
        $product = (new Product());
        $product->name = $dto->name;
        $product->description = $dto->description;
        $product->save();


        foreach ($dto->attributes as $attributeName => $attributeValues) {
            $attribute = Attribute::firstOrCreate([
                'name' => $attributeName,
            ]);


            foreach ($attributeValues['values'] as $value) {
                $attributeValue = AttributeValue::firstOrCreate([
                    'value' => $value,
                    'attribute_id' => $attribute->id,
                ]);

                ProductAttributeValue::firstOrCreate([
                    'product_id' => $product->id,
                    'attribute_id' => $attribute->id,
                    'attribute_value_id' => $attributeValue->id,
                ]);
            }
        }

        return $product;
    }
}
