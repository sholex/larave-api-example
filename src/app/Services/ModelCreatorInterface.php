<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Spatie\DataTransferObject\DataTransferObject;

interface ModelCreatorInterface
{
    public function create(DataTransferObject $dataTransferObject): Model;
}
