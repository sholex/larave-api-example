<?php

namespace App\Http\Requests\Brand;

use App\Http\Requests\AbstractRequest;
use App\Models\Brand;
use Illuminate\Validation\Rule;
use Spatie\DataTransferObject\DataTransferObject;

class UpdateBrandRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                Rule::unique(Brand::class)->ignore($this->brand)
            ]
        ];
    }

    public function data(): DataTransferObject
    {
        // TODO: Implement data() method.
    }
}
