<?php

declare(strict_types=1);

namespace App\Http\Requests\Product;

use App\Http\Requests\AbstractRequest;
use App\Http\Requests\Product\Dto\CreateProductDto;

class CreateProductRequest extends AbstractRequest
{
    private const NAME = 'name';
    private const DESCRIPTION = 'description';
    private const ATTRIBUTES = 'attributes';

    public function rules()
    {
        return [
            self::NAME => [
                'required',
                'string',
            ],
            self::DESCRIPTION => [
                'string',
                'nullable'
            ],
            self::ATTRIBUTES => [
                'array'
            ],
        ];
    }

    public function data(): CreateProductDto
    {
        return new CreateProductDto(
            name: $this->input(self::NAME),
            description: $this->input(self::DESCRIPTION) ?? "",
            attributes: $this->input(self::ATTRIBUTES) ?? [],
        );
    }
}
