<?php

declare(strict_types=1);

namespace App\Http\Requests\Product\Dto;

use Spatie\LaravelData\Data;

class CreateProductDto extends Data
{
    public function __construct(
        public string $name,
        public string $description,
        public array $attributes,
    ) {
    }
}
