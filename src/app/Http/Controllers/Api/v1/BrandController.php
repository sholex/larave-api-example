<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Brand\BrandListRequest;
use App\Http\Requests\Brand\CreateBrandRequest;
use App\Http\Requests\Brand\UpdateBrandRequest;
use App\Http\Resources\Brand\BrandBaseResource;
use App\Models\Brand;
use Illuminate\Http\JsonResponse;

class BrandController extends Controller
{
    /**
     * @param BrandListRequest $request
     * @return JsonResponse
     */
    public function index(BrandListRequest $request): JsonResponse
    {
        return response()->json(BrandBaseResource::collection(Brand::all()));
    }

    public function store(CreateBrandRequest $request): JsonResponse
    {
        $brand = Brand::create($request->validated());

        return response()->json(new BrandBaseResource($brand), 201);
    }

    public function show(Brand $brand): JsonResponse
    {
        return response()->json(new BrandBaseResource($brand));
    }

    public function update(UpdateBrandRequest $request, Brand $brand): JsonResponse
    {
        $brand->update($request->validated());

        return response()->json($brand);
    }

    public function destroy(Brand $brand)
    {
        $brand->delete();

        return response()->noContent();
    }
}
