<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Brand\UpdateBrandRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Resources\Brand\BrandBaseResource;
use App\Http\Resources\Product\ProductBaseResource;
use App\Http\Resources\Product\ProductSimpleResource;
use App\Models\Brand;
use App\Models\Product;
use App\Services\Product\ProductCreator;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    private ProductCreator $productCreator;

    public function __construct(ProductCreator $productCreator)
    {
        $this->productCreator = $productCreator;
    }

    public function index(): JsonResponse
    {
        return response()->json(ProductSimpleResource::collection(Product::all()));
    }

    public function store(CreateProductRequest $request): JsonResponse
    {
        return response()->json(new ProductSimpleResource(
            $this->productCreator->create($request->data())
        ), Response::HTTP_CREATED);
    }

    public function show(Brand $brand): JsonResponse
    {
        return response()->json(new BrandBaseResource($brand));
    }

    public function update(UpdateBrandRequest $request, Brand $brand): JsonResponse
    {
        $brand->update($request->validated());

        return response()->json($brand);
    }

    public function destroy(Brand $brand)
    {
        $brand->delete();

        return response()->noContent();
    }
}
