<?php

declare(strict_types=1);

namespace App\Http\Resources\ProductAttributeValue;

use App\Models\AttributeValue;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductAttributeValue;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductAttributeValueSimpleResource extends ProductAttributeValueBaseResource
{
    public function toArray($request): array
    {
        /** @var ProductAttributeValue $this */
        return array_merge(
            parent::toArray($request),
            [
                'attribute_name' => $this->attribute->name,
                'attribute_value' => $this->attributeValue->value,
            ]
        );
    }
}
