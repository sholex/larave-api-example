<?php

declare(strict_types=1);

namespace App\Http\Resources\ProductAttributeValue;

use App\Models\Brand;
use App\Models\ProductAttributeValue;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductAttributeValueBaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var ProductAttributeValue $this */
        return [
//            'id' => $this->id,
        ];
    }
}
