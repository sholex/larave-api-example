<?php

declare(strict_types=1);

namespace App\Http\Resources\Brand;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandBaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Brand $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
