<?php

declare(strict_types=1);

namespace App\Http\Resources\Product;

use App\Http\Resources\ProductCategory\ProductCategorySimpleResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductBaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray(Request $request): array
    {
//        var_dump($this->productCategories);
//        die();
        /** @var Product $this */
        return [
            'id' => $this->id,
        ];
    }
}
