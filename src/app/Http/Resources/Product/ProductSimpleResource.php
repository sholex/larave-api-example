<?php

declare(strict_types=1);

namespace App\Http\Resources\Product;

use App\Http\Resources\ProductAttributeValue\ProductAttributeValueSimpleResource;
use App\Http\Resources\ProductCategory\ProductCategorySimpleResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductSimpleResource extends ProductBaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var Product $this */
        return array_merge(
            parent::toArray($request),
            [
                'name' => $this->name,
                'description' => $this->description,
                'attributes' => ProductAttributeValueSimpleResource::collection($this->productAttributeValues),
                'categories' => ProductCategorySimpleResource::collection($this->productCategories),
            ]
        );
    }
}
