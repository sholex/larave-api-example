<?php

declare(strict_types=1);

namespace App\Http\Resources\ProductCategory;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategoryBaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var ProductCategory $this */
        return [
            'id' => $this->id,
        ];
    }
}
