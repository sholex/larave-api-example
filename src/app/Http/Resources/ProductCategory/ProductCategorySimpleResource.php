<?php

declare(strict_types=1);

namespace App\Http\Resources\ProductCategory;

use App\Http\Resources\ProductAttributeValue\ProductAttributeValueSimpleResource;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategorySimpleResource extends ProductCategoryBaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var ProductCategory $this */
        return array_merge(
            parent::toArray($request),
            [
                'name' => $this->name,
            ]
        );
    }
}
