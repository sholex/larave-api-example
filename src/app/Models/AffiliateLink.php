<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AffiliateLink extends Model
{
    use HasUuids;

    protected $fillable = [
        'product_id',
        'shop_id',
        'name',
        'url',
        'price',
        'discount_price',//?
        'discount',//?
        'quantity',//?
        'status',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function shop():BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
/**
 *
Attribute
-Size
AttrValue
-S
-M

products_attributes_values
-product_id
-attribute_id
-attribute_value_id

 */
}
