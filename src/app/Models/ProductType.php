<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    public const SIMPLE = 'SIMPLE';
    public const SPARE_PART = 'SPARE_PART';
    public const COMPLEX = 'COMPLEX';

    public const LIST = [
        self::SIMPLE        => 'Simple',
        self::SPARE_PART    => 'Spare part',
        self::COMPLEX       => 'Complex',
    ];
}
