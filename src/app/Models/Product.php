<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\Filterable;
use DateTime;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property string $id
 * @property ?string $name
 * @property ?string $description
 * @property BelongsToMany $productCategories
 * @property HasMany $productAttributeValues
 * @property ?DateTime $created_at
 * @property ?DateTime $updated_at
 */
class Product extends Model
{
    use HasUuids;
    use Filterable;
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
    ];

    public function affiliateLinks(): HasMany
    {
        return $this->hasMany(AffiliateLink::class);
    }

    public function productAttributeValues(): HasMany
    {
        return $this->hasMany(ProductAttributeValue::class);
    }

    public function productCategories(): BelongsToMany
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function brand(): HasOne
    {
        return $this->hasOne(Brand::class);
    }
}
