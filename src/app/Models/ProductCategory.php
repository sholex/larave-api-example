<?php

declare(strict_types=1);

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property string $id
 * @property ?string $name
 * @property ?BelongsToMany $products
 * @property ?DateTime $created_at
 * @property ?DateTime $updated_at
 */
class ProductCategory extends Model
{
    use HasUuids;

    protected $fillable = [
        'name',
        'parent_category_id'
    ];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}
