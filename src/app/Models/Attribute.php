<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property string $name
 * @property HasMany $attributesValue
 *
 * @mixin Builder
 */
class Attribute extends Model
{
    use HasUuids;

    protected $fillable = [
        'name'
    ];

    public function attributesValues(): HasMany
    {
        return $this->hasMany(AttributeValue::class);
    }
}
