<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\AttributeValue
 *
 * @property string $id
 * @property string $value
 * @property BelongsTo $attribute
 *
 * @mixin Builder
 */
class AttributeValue extends Model
{
    use HasUuids;

    protected $fillable = [
        'value',
        'attribute_id',
    ];

    public function attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class);
    }
}
