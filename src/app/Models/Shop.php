<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * App\Brand
 *
 * @property string $id
 * @property ?string $name
 * @property HasManyThrough $products
 * @property HasMany $affiliateLinks
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Shop extends Model
{
    use HasUuids;

    protected $fillable = [
        'name'
    ];

    public function products(): HasManyThrough
    {
        return $this->hasManyThrough(Product::class, AffiliateLink::class);
    }

    public function affiliateLinks(): HasMany
    {
        return $this->hasMany(AffiliateLink::class);
    }
}


