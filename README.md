# Laravel api example

## Installation

**Clone repository**
- `git clone git@gitlab.com:sholex/larave-api-example.git`

**Start Docker containers**
- `docker-compose up -d`

**Install dependencies**
- `docker-compose run --rm composer install`

**Database setup**
create .env file from .env.example

run `docker-compose run --rm artisan key:generate` to generate app secret key

enter database credentials from docker-compose.yml

by default they are:
```dotenv
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

**Run migrations & seeds**
- `docker-compose run --rm artisan migrate --seed`

**Run Laravel Passport**
- `docker-compose run --rm artisan passport:install`

**Run queues worker**
- `docker-compose run --rm artisan queue:work`

After all this steps you can open application at **http://localhost:8085**


## API usage
You can use `./rest-api_participants_requests.http` file to test all existing api endpoints


#### Login as existing user
POST http://localhost:8085/api/v1/login
{
  "email": "admin@mail.com",
  "password": "root"
}

#### OR register as new


#### Fetch
You can use as parameter any field from $fillable variable of Participant model

PUT http://localhost:8085/api/v1/participants/{participant_id}

PUT http://localhost:8085/api/v1/participants/{participant_id}/?event_id=1

PUT http://localhost:8085/api/v1/participants/{participant_id}/?first_name=Peter

PUT http://localhost:8085/api/v1/participants/{participant_id}/?event_id=1&first_name=Peter


#### Show participant by id
GET http://localhost:8085/api/v1/participants/{participant_id}


#### Update participant by id
PUT http://localhost:8085/api/v1/participants/{participant_id}

You can update just 1 field:
```json
{
  "first_name": "string",
  "last_name": "string",
  "email": "email",
  "event_id": "integer"
}
```


#### Create participant
POST http://localhost:8085/api/v1/participants/

```json
{
  "first_name": "string",
  "last_name": "string",
  "email": "email",
  "event_id": "integer"
}
```

After new participant been created new job executed, simulating email sending.

You can see results in logs.

### Delete participant
DELETE http://localhost:8085/api/v1/participants/{participant_id}


### TESTS

Enter container

`docker exec -it php //bin/sh`

run tests

`./vendor/bin/phpunit -v -c ./phpunit.xml`

OR you can setup PHPStorm to run tests directly

##Configure PhpStorm

### Add php interpreter from docker image
Configure run test in container.
+ *File | Settings | Languages & Frameworks | PHP | CLI Interpreter | Select(..)*
    + Add | From Docker, Vagrant, VM, Remote
        + Docker Compose: +
        + Server: 
            + New
                + Name: Docker
                + Unix socket: +
        + Configuration file(s): ./docker-compose.yml
        + Service: php
        + PHP executable: php

**This configuration allows to run and debug tests in environment of image.**

## General App Usage

Composer, NPM, and Artisan commands without having to have these platforms installed on your local computer. Use the following command templates from your project root, modifiying them to fit your particular use case:
- `docker-compose run --rm composer update`
- `docker-compose run --rm npm run dev`
- `docker-compose run --rm artisan migrate` 

Containers created and their ports (if used) are as follows:

- **nginx** - `:8080`
- **mysql** - `:3306`
- **php** - `:9000`
- **npm**
- **composer**
- **artisan**

## Persistent MySQL Storage

By default, whenever you bring down the docker-compose network, your MySQL data will be removed after the containers are destroyed. If you would like to have persistent data that remains after bringing containers down and back up, do the following:

1. Create a `mysql` folder in the project root, alongside the `nginx` and `src` folders.
2. Under the mysql service in your `docker-compose.yml` file, add the following lines:

```
volumes:
  - ./mysql:/var/lib/mysql
```

